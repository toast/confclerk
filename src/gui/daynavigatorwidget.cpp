/*
 * Copyright (C) 2010 Ixonos Plc.
 * Copyright (C) 2011-2024 Philipp Spitzer, gregor herrmann, Stefan Stahl
 *
 * This file is part of ConfClerk.
 *
 * ConfClerk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * ConfClerk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ConfClerk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPainter>
#include <QLabel>

#include "daynavigatorwidget.h"
#include "conference.h"
#include "application.h"


DayNavigatorWidget::DayNavigatorWidget(QWidget *aParent): QWidget(aParent) {
    setupUi(this);    

    connect(prevDayButton, SIGNAL(clicked()), SLOT(prevDayButtonClicked()));
    connect(nextDayButton, SIGNAL(clicked()), SLOT(nextDayButtonClicked()));

    configureNavigation();
}


void DayNavigatorWidget::setDates(const QDate &aStartDate, const QDate &aEndDate) {
    Q_ASSERT(aStartDate.isValid() && aEndDate.isValid() && aStartDate<=aEndDate);

    mStartDate = aStartDate;
    mEndDate = aEndDate;

    if (!mCurDate.isValid()) mCurDate = mStartDate;
    // if mCurDate is out of range, set it to mstartDate
    else if (mCurDate < mStartDate || mCurDate > mEndDate) mCurDate = mStartDate;

    configureNavigation();
    emit(dateChanged(mCurDate));
    this->update();
}


void DayNavigatorWidget::setCurDate(const QDate& curDate) {
    Q_ASSERT(curDate.isValid());
    if (curDate == mCurDate) return;

    if (!mStartDate.isValid()) {
        // the start and end date have not been set
        mStartDate = curDate;
        mEndDate = curDate;
        mCurDate = curDate;
    }

    else if (curDate < mStartDate) mCurDate = mStartDate;
    else if (curDate > mEndDate) mCurDate = mEndDate;
    else mCurDate = curDate;

    configureNavigation();
    emit(dateChanged(mCurDate));
    this->update();
}


void DayNavigatorWidget::unsetDates() {
    mStartDate= QDate();
    mEndDate = QDate();
    mCurDate = QDate();

    configureNavigation();
    emit(dateChanged(mCurDate));
    this->update();
}

QSize DayNavigatorWidget::minimumSizeHint() const {
#ifdef MAEMO
    return QSize(20, 0);
#else
    return QSize(fontMetrics().lineSpacing() * 3 / 2, 0);
#endif
}


void DayNavigatorWidget::configureNavigation() {
    prevDayButton->setDisabled(!mStartDate.isValid() || mCurDate == mStartDate);
    nextDayButton->setDisabled(!mEndDate.isValid() || mCurDate == mEndDate);
}


void DayNavigatorWidget::prevDayButtonClicked() {
    if(mCurDate <= mStartDate) return;
    mCurDate = mCurDate.addDays(-1);
    configureNavigation();
    emit(dateChanged(mCurDate));
    this->update();
}


void DayNavigatorWidget::nextDayButtonClicked() {
    if(mCurDate >= mEndDate) return;
    mCurDate = mCurDate.addDays(1);
    configureNavigation();
    emit(dateChanged(mCurDate));
    this->update();
}


void DayNavigatorWidget::paintEvent(QPaintEvent *aEvent) {
    Q_UNUSED(aEvent);
    QString selectedDateStr;
    if (mCurDate.isValid()) {
        QString selectedDateFormat =
#ifdef MAEMO
                "dddd\nyyyy-MM-dd";
#else
                "dddd • yyyy-MM-dd";
#endif
        selectedDateStr = mCurDate.toString(selectedDateFormat);
        bool hasConference = ((Application*) qApp)->hasActiveConference();
        if (hasConference) {
            Conference& conference = ((Application*) qApp)->activeConference();
            if (conference.hasDisplayTimeShift() && conference.displayTimeShift() != 0) {
                QTime shift(0, 0);
                bool minus = conference.displayTimeShift() < 0;
                shift = shift.addSecs(conference.displayTimeShift() * 60 * (minus ? -1 : 1));
                selectedDateStr += " • " + (minus ? QString("-") : "+") + shift.toString("HH:mm");
            }
        }
    } else {
        selectedDateStr = tr("No date");
    }
    QPainter painter(this);
    painter.save();

    // rectangle only for the text
    QRect q(-selectedDate->height()-selectedDate->y(), selectedDate->x(), selectedDate->height(), selectedDate->width());
    painter.rotate(270);

    // font size adjustion, static on maemo, dynamically otherwise
    QFont f = painter.font();
#ifdef MAEMO
    qreal factor = 0.8;
#else
#if QT_VERSION >= 0x050b00 // QT 5.11
    auto dateStrWidth = painter.fontMetrics().horizontalAdvance(selectedDateStr);
#else
    auto dateStrWidth = painter.fontMetrics().width(selectedDateStr);
#endif
    qreal factor = (qreal) 2 * q.width() / dateStrWidth;
#endif
    if (factor < 1) f.setPointSizeF(f.pointSizeF() * factor);
    painter.setFont(f);

    painter.drawText(q, Qt::AlignCenter, selectedDateStr);
    painter.restore();
}
